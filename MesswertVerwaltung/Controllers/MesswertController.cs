﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MesswertVerwaltung.Models;
using System.Globalization;

namespace MesswertVerwaltung.Controllers
{
    public class MesswertController : Controller
    {
        private MesswertDB db = new MesswertDB();

        // GET: /Messwert/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var messwertset = db.MesswertSet.Include(v => v.Messwerttypen);
            return View(messwertset.ToList());
        }

        // GET: /Messwert/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerte messwerte = db.MesswertSet.Find(id);
            if (messwerte == null)
            {
                return HttpNotFound();
            }
            return View(messwerte);
        }

        // GET: /Messwert/Create
        public ActionResult Create()
        {
            ViewBag.MesswerttypenId = new SelectList(db.MesswerttypenSet, "Id", "Messwerttypname");
            return View();
        }

        // POST: /Messwert/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MesswerttypenId,Sensorname,Datum,Messwert")] Messwerte messwerte)
        {
            DateTime time = Convert.ToDateTime(Request.Form["time"]);

            if (ModelState.IsValid)
            {
                messwerte.Datum = time;
                db.MesswertSet.Add(messwerte);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messwerte);
        }

        // GET: /Messwert/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerte messwerte = db.MesswertSet.Find(id);
            if (messwerte == null)
            {
                return HttpNotFound();
            }
            ViewBag.MesswerttypenId = new SelectList(db.MesswerttypenSet, "Id", "Messwerttypname");
            return View(messwerte);
        }

        // POST: /Messwert/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MesswerttypenId,Sensorname,Datum,Messwert")] Messwerte messwerte)
        {
            Funktions funktions = new Funktions();
            DateTime time = Convert.ToDateTime(Request.Form["time"]);

            if (ModelState.IsValid)
            {
                messwerte.Datum = Convert.ToDateTime(funktions.AssembleDateTime(messwerte.Datum, time));
                db.Entry(messwerte).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MesswerttypenId = new SelectList(db.MesswerttypenSet, "Id", "Messwerttypname");
            return View(messwerte);
        }

        // GET: /Messwert/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerte messwerte = db.MesswertSet.Find(id);
            if (messwerte == null)
            {
                return HttpNotFound();
            }
            return View(messwerte);
        }

        // POST: /Messwert/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messwerte messwerte = db.MesswertSet.Find(id);
            db.MesswertSet.Remove(messwerte);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Index(int? id)
        {
            Funktions funktions = new Funktions();

            int messwerttyp = -1;
            string sensorname = "";
            DateTime datumVon = DateTime.Parse("01.01.0001");
            DateTime zeitVon = DateTime.Parse("01.01.0001 00:00:00");
            DateTime fullDateVon;
            DateTime datumBis = DateTime.Parse("01.01.0001");
            DateTime zeitBis = DateTime.Parse("01.01.0001 00:00:00");
            DateTime fullDateBis;
            double messwertVon = 0.0;
            double messwertBis = 0.0;

            //bool filterCorrect = true;

            List<Messwerte> listOrdered = new List<Messwerte>();

            if (Request.Form["messwerttyp"] != "")
                messwerttyp = Convert.ToInt32(Request.Form["messwerttyp"]);

            if (!String.IsNullOrEmpty(Request.Form["sensorname"]))
                sensorname = Request.Form["sensorname"];

            if (!String.IsNullOrEmpty(Request.Form["datumVon"]))
                datumVon = Convert.ToDateTime(Request.Form["datumVon"]);

            if (!String.IsNullOrEmpty(Request.Form["zeitVon"]))
                zeitVon = Convert.ToDateTime(Request.Form["zeitVon"]);

            fullDateVon = Convert.ToDateTime(funktions.AssembleDateTime(datumVon, zeitVon));

            if (!String.IsNullOrEmpty(Request.Form["datumBis"]))
                datumBis = Convert.ToDateTime(Request.Form["datumBis"]);

            if (!String.IsNullOrEmpty(Request.Form["zeitBis"]))
                zeitBis = Convert.ToDateTime(Request.Form["zeitBis"]);

            fullDateBis = Convert.ToDateTime(funktions.AssembleDateTime(datumBis, zeitBis));

            if (!String.IsNullOrEmpty(Request.Form["messwertVon"]))
            {
                if (Request.Form["messwertVon"].Contains("."))
                    messwertVon = Double.Parse(Request.Form["messwertVon"].Replace(".", ","));
                else
                    messwertVon = Double.Parse(Request.Form["messwertVon"]);
            }

            if (!String.IsNullOrEmpty(Request.Form["messwertBis"])) 
            {
                if(Request.Form["messwertBis"].Contains("."))
                    messwertBis = Double.Parse(Request.Form["messwertBis"].Replace(".", ","));
                else
                    messwertBis = Double.Parse(Request.Form["messwertBis"]);
            }

            var messwerte = db.MesswertSet.AsQueryable<Messwerte>();

            if (messwerttyp != -1)
            {
                messwerte = messwerte.Where(mw => mw.MesswerttypenId.Equals(messwerttyp));
            }

            if (!String.IsNullOrEmpty(sensorname))
                messwerte = messwerte.Where(mw => mw.Sensorname.Contains(sensorname));

            if (fullDateVon != DateTime.Parse("01.01.0001 00:00:00") && fullDateBis == DateTime.Parse("01.01.0001 00:00:00"))
            {
                messwerte = messwerte.Where(mw => mw.Datum.Equals(fullDateVon));
            }
            else if (fullDateVon == DateTime.Parse("01.01.0001 00:00:00") && fullDateBis != DateTime.Parse("01.01.0001 00:00:00"))
            {
                messwerte = messwerte.Where(mw => mw.Datum.Equals(fullDateBis));
            }
            else if (fullDateVon != DateTime.Parse("01.01.0001 00:00:00") && fullDateBis != DateTime.Parse("01.01.0001 00:00:00"))
            {
                if (fullDateVon < fullDateBis)
                {
                    foreach (var item in messwerte)
                    {
                        if (item.Datum >= fullDateVon)
                        {
                            if (item.Datum <= fullDateBis)
                            {
                                listOrdered.Add(item);
                            }
                        }
                        else if (fullDateBis <= item.Datum)
                        {
                            if (fullDateVon >= item.Datum)
                            {
                                listOrdered.Add(item);
                            }
                        }
                    }

                    messwerte = listOrdered.AsQueryable();
                }
            }

            if (messwertVon == 0.0 && messwertBis != 0.0)
            {
                messwerte = messwerte.Where(mw => mw.Messwert.Equals(messwertBis));
            }
            else if (messwertVon != 0.0 && messwertBis == 0.0)
            {
                messwerte = messwerte.Where(mw => mw.Messwert.Equals(messwertVon));
            }
            else if (messwertVon != 0.0 && messwertBis != 0.0)
            {
                if (messwertVon < messwertBis)
                {

                    foreach (var item in messwerte)
                    {
                        if (item.Messwert >= messwertVon)
                        {
                            if (item.Messwert <= messwertBis)
                            {
                                listOrdered.Add(item);
                            }
                        }
                        else if (messwertBis <= item.Messwert)
                        {
                            if (messwertVon >= item.Messwert)
                            {
                                listOrdered.Add(item);
                            }
                        }
                    }

                    messwerte = listOrdered.AsQueryable();
                }
            }

            if (messwerttyp == -1 && String.IsNullOrEmpty(sensorname) && fullDateVon == DateTime.Parse("01.01.0001 00:00:00") && fullDateBis == DateTime.Parse("01.01.0001 00:00:00") && messwertVon == 0.0 && messwertBis == 0.0)
                return RedirectToAction("Index");
            else
                return View(messwerte);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
