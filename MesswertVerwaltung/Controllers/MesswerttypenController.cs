﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MesswertVerwaltung.Models;

namespace MesswertVerwaltung.Controllers
{
    public class MesswerttypenController : Controller
    {
        private MesswertDB db = new MesswertDB();

        // GET: /Messwerttypen/
        public ActionResult Index()
        {
            return View(db.MesswerttypenSet.ToList());
        }

        // GET: /Messwerttypen/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerttypen messwerttypen = db.MesswerttypenSet.Find(id);
            if (messwerttypen == null)
            {
                return HttpNotFound();
            }
            return View(messwerttypen);
        }

        // GET: /Messwerttypen/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Messwerttypen/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Messwerttypname")] Messwerttypen messwerttypen)
        {
            if (ModelState.IsValid)
            {
                db.MesswerttypenSet.Add(messwerttypen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messwerttypen);
        }

        // GET: /Messwerttypen/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerttypen messwerttypen = db.MesswerttypenSet.Find(id);
            if (messwerttypen == null)
            {
                return HttpNotFound();
            }
            return View(messwerttypen);
        }

        // POST: /Messwerttypen/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Messwerttypname")] Messwerttypen messwerttypen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messwerttypen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messwerttypen);
        }

        // GET: /Messwerttypen/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messwerttypen messwerttypen = db.MesswerttypenSet.Find(id);
            if (messwerttypen == null)
            {
                return HttpNotFound();
            }
            return View(messwerttypen);
        }

        // POST: /Messwerttypen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Messwerttypen messwerttypen = db.MesswerttypenSet.Find(id);
            db.MesswerttypenSet.Remove(messwerttypen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
