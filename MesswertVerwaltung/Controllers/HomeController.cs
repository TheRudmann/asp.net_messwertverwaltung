﻿using MesswertVerwaltung.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MesswertVerwaltung.Controllers
{
    public class HomeController : Controller
    {
        private MesswertDB context = new MesswertDB();
        public ActionResult Index()
        {
            var messwerte = context.MesswertSet
                .Include("Messwerttypen").ToList();
            return View(messwerte);
        }

        [HttpPost]
        public ActionResult Upload(Data data) 
        {
            Funktions funktions = new Funktions();
            if (data.UploadFile != null && data.UploadFile.ContentLength > 0)
            {
                var allowedExtensions = new[] { ".txt", ".mwf" };
                var extension = Path.GetExtension(data.UploadFile.FileName);

                if (!allowedExtensions.Contains(extension))
                    return Content("Bitte wählen Sie ein .txt oder .mvf File aus!");
                else
                {
                    var fileName = Path.GetFileName(data.UploadFile.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Files"), fileName);
                    data.UploadFile.SaveAs(path);

                    funktions.ParseFileContent(path);
                    //var fileContent = System.IO.File.ReadAllText(path);

                    return RedirectToAction("Index");
                }
            }

            return Content("Bitte wählen Sie eine Datei aus!");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}