﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using MesswertVerwaltung.Models;

namespace MesswertVerwaltung
{
    public class Funktions
    {
        private MesswertDB db = new MesswertDB();

        public Funktions()
        {

        }

        public string AssembleDateTime(DateTime date, DateTime time)
        {
            string tmpDate, tmpTime, fullDateString;

            tmpDate = date.ToShortDateString();
            tmpTime = time.ToShortTimeString();
            fullDateString = tmpDate + " " + tmpTime;

            return fullDateString;
        }

        public void ParseFileContent(string path)
        {
            int anzahlMesswerte;
            string sWert = "";

            foreach (string line in File.ReadLines(path))
            {
                Messwerte messwerte = new Messwerte();
                Messwerttypen messwerttypen = new Messwerttypen();

                messwerte.Sensorname = line.Substring(0, 29);
                messwerttypen.Messwerttypname = line.Substring(30, 30);
                anzahlMesswerte = Int32.Parse(line.Substring(60, 4));

                int startPos = 64;

                for (int i = 1; i <= anzahlMesswerte; i++)
                {
                    sWert = line.Substring(startPos, 14);
                    startPos += 15;
                    messwerte.Datum = ParseStringToDateTime(sWert);

                    sWert = line.Substring(startPos, 19).Replace(".", ",");
                    startPos += 19;

                    messwerte.Messwert = Double.Parse(sWert);

                    var mw = new Messwerttypen { Messwerttypname = messwerttypen.Messwerttypname, Name = new List<Messwerte>() };

                    mw.Name.Add(new Messwerte
                    {
                        Sensorname = messwerte.Sensorname,
                        Datum = messwerte.Datum,
                        Messwert = messwerte.Messwert
                    });

                    db.MesswerttypenSet.Add(mw);
                    db.SaveChanges();
                }
            }
        }

        public DateTime ParseStringToDateTime(string dateString)
        {
            DateTime date = new DateTime();
            string tmp = dateString;

            dateString = tmp.Substring(0, 4) + ".";
            dateString += tmp.Substring(4, 2) + ".";
            dateString += tmp.Substring(6, 2) + " ";
            dateString += tmp.Substring(8, 2) + ":";
            dateString += tmp.Substring(10, 2) + ":";
            dateString += tmp.Substring(12, 2);

            date = DateTime.Parse(dateString);

            return date;
        }
    }
}