﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MesswertVerwaltung.Startup))]
namespace MesswertVerwaltung
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
