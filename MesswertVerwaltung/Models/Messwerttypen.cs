﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MesswertVerwaltung.Models
{
    [Table("Messwerttypen")]
    public class Messwerttypen
    {
        public int Id { get; set; }

        public String Messwerttypname { get; set; }

        public virtual ICollection<Messwerte> Name { get; set; }
    }
}