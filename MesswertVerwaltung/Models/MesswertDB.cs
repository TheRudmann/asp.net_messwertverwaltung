﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MesswertVerwaltung.Models
{
    public class MesswertDB : DbContext
    {
        static MesswertDB()
        {
            Database.SetInitializer(new MyInitializer());
        }

        public DbSet<Messwerte> MesswertSet { get; set; }
        public DbSet<Messwerttypen> MesswerttypenSet { get; set; }
    }

    public class MyInitializer : DropCreateDatabaseIfModelChanges<MesswertDB> 
    {
        protected override void Seed(MesswertDB context)
        {
            //context.MesswertSet.Add(new Messwerte { Messwerttyp = "Temperatur", Sensorname = "Nickel 1000", Datum = DateTime.Today, Messwert = 18.123532 });
            //context.MesswertSet.Add(new Messwerte { Messwerttyp = "Temperatur", Sensorname = "Nickel 1000", Datum = DateTime.Today.AddDays(1), Messwert = 18.123532 });
            //context.MesswertSet.Add(new Messwerte { Messwerttyp = "Temperatur", Sensorname = "Nickel 1000", Datum = DateTime.Today.AddDays(2), Messwert = 18.123532 });

            
            //var ktn = new Region { Name = "Kärnten ", Vorhersagen = new List<Vorschau>() };

            //stmk.Vorhersagen.Add(new Vorschau
            //{
            //    Datum = DateTime.Today.AddDays(1),
            //    Text = "Bis Mittag Regnerisch"
            //});

            //var stmk = new Region { Name = "Steiermark", Vorhersagen = new List<Vorschau>() };

            var mw1 = new Messwerttypen { Messwerttypname = "MWT1234", Name = new List<Messwerte>() };
            var mw2 = new Messwerttypen { Messwerttypname = "MWT2143", Name = new List<Messwerte>() };
            var mw3 = new Messwerttypen { Messwerttypname = "MWT4231", Name = new List<Messwerte>() };

            mw1.Name.Add(new Messwerte
            {
                Sensorname = "Sensor1",
                Datum = DateTime.Today,
                Messwert = 18.123532
            });

            mw2.Name.Add(new Messwerte
            {
                Sensorname = "Sensor2",
                Datum = DateTime.Today,
                Messwert = 18.123532
            });

            mw3.Name.Add(new Messwerte
            {
                Sensorname = "Sensor3",
                Datum = DateTime.Today,
                Messwert = 18.123532
            });

            context.MesswerttypenSet.Add(mw1);
            context.MesswerttypenSet.Add(mw2);
            context.MesswerttypenSet.Add(mw3);
            context.SaveChanges();
            base.Seed(context);
        }
    }
}