﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MesswertVerwaltung.Models
{
    [Table("Messwerte")]
    public class Messwerte
    {
        public int Id { get; set; }

        [ForeignKey("Messwerttypen")]
        [Display(Name = "Messwerttypen")]
        public int MesswerttypenId { get; set; }

        public String Sensorname { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime Datum { get; set; }

        public Double Messwert { get; set; }

        public virtual Messwerttypen Messwerttypen { get; set; }
    }
}